import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;

//////////////////////////////			MINIM			//////////////////////////////
Minim minim;
// for recording
AudioInput input;
AudioRecorder recorder;
// for playing back
AudioOutput output;
FilePlayer fp;
AudioPlayer ap;
AudioPlayer env;
AudioPlayer spring;
AudioPlayer loop;

//////////////////////////////			CUSTOM			//////////////////////////////
Player casio;
Part root; // used to simulate an object positioned in 0,0, much simpler than making parent an optional parameter
Part body;
Part controller;
Part side;
Part cForward;
Part cRewind;
Part cPlay;
Part cRec;
Part cPause;
Part tCustom;
Part tGreenday;
Part tGross;
Part tRain;

Bound[] bounds = new Bound[9];

String clicked = "none";

//////////////////////////////			SETUP			//////////////////////////////
void setup() {
	//size(displayWidth,displayHeight, P3D);
	size(1150, 750, P2D);

	// minim init
	minim = new Minim(this);
	input = minim.getLineIn(Minim.STEREO, 2048);
	output = minim.getLineOut( Minim.STEREO );

	ap = minim.loadFile("data/tape_custom.wav");

	// custom classes init
	casio = new Player();

	root = new Part ("root","btn_play_off.png",0,0,root); //cheating with parent but it works so...
	body = new Part("body","body_full.png",90,20,root);
	controller = new Part("controller","controller.png",30,432,body);
	side = new Part("side","side.png",650,0,body);
	cForward =  new Part("cForward","btn_forward_off.png",260,69,controller);
	cRewind =  new Part("cRewind","btn_rewind_off.png",319,69,controller);
	cPlay =  new Part("cPlay","btn_play_off.png",378,61,controller);
	cRec =  new Part("cRec","btn_rec_off.png",449,69,controller);
	cPause =  new Part("cPause","btn_stop_off.png",55,110,side);

	tCustom =  new Part("tCustom","tape_custom.png",10,650,root);
	tGreenday =  new Part("tGreenday","tape_greenday.png",285,0,tCustom);
	tGross =  new Part("tGross","tape_gross.png",285,0,tGreenday);
	tRain =  new Part("tRain","tape_rain.png",285,0,tGross);

	bounds[0] = new Bound(cForward);
	bounds[1] = new Bound(cRewind);
	bounds[2] = new Bound(cPlay);
	bounds[3] = new Bound(cRec);
	bounds[4] = new Bound(cPause);
	bounds[5] = new Bound(tCustom);
	bounds[6] = new Bound(tGreenday);
	bounds[7] = new Bound(tGross);
	bounds[8] = new Bound(tRain);

}

//////////////////////////////			DRAW			//////////////////////////////

void draw() {
	background(255);
	fill(0);

	body.display();
	controller.display();
	side.display();
	cForward.display();
	cRewind.display();
	cPlay.display();
	cRec.display();
	cPause.display();

	tCustom.display();
	tGreenday.display();
	tGross.display();
	tRain.display();

	casio.displayStatus();
	casio.drawBar();
	casio.updateTime();
	casio.tick();
	switch (clicked) {
	case "none":
		break;
	case "cForward":
		casio.forward();
		break;
	case "cRewind":
		casio.rewind();
		break;
	case "cPlay":
		casio.play();
		break;
	case "cRec":
		if (casio.selectedTape == "custom") {
			casio.record();
		}
		break;
	case "cPause":
		casio.pause();
		cPause.changeImg("btn_stop_on.png");
		break;
	case "tCustom":
		casio.changeTape("custom");
		break;
	case "tGreenday":
		casio.changeTape("greenday");
		break;
	case "tGross":
		casio.changeTape("gross");
		break;
	case "tRain":
		casio.changeTape("rain");
		break;
	}
	clicked = "none";
}

//////////////////////////////			MOUSEWHEEL			//////////////////////////////

void mouseWheel(MouseEvent event) {
	/*float e = event.getCount();
	   playerVolume -= e;
	   float playerGain = map(playerVolume, 0, 100, -45, 5);
	   ap.setGain(playerGain);
	   println(playerGain);*/
	float e = event.getCount();
	casio.volume -= e;
	float playerGain = map(casio.volume, 0, 100, -45, 5);
	ap.setGain(playerGain);
	//println(playerGain);
}

//////////////////////////////			CLICK			//////////////////////////////

void mousePressed(){
	for (int i = 0; i < bounds.length; i++) {
		bounds[i].x1 = bounds[i].part.finalX;
		bounds[i].x2 = bounds[i].part.finalX+bounds[i].part.img.width;
		bounds[i].y1 = bounds[i].part.finalY;
		bounds[i].y2 = bounds[i].part.finalY+bounds[i].part.img.height;
		if (mouseX >= bounds[i].x1 && mouseX <= bounds[i].x2 && mouseY >= bounds[i].y1 && mouseY <= bounds[i].y2) {
			clicked = bounds[i].part.name;
		}
	}
}
void mouseReleased(){
	cPause.changeImg("btn_stop_off.png");
}


//////////////////////////////			KEYPRESS			//////////////////////////////
void keyReleased()
{

	//print(key);

	if (key == CODED) {

		//print(keyCode);

		if (keyCode == 16) {   //shift
			//casio.pause();
		}

		switch (keyCode) {
		case 37:   //left
			casio.rewind();
			break;
		case 39:   //right
			casio.forward();
			break;
		}
	}

	switch (key) {
	case '@':
		casio.changeTape("bonus");
		break;
	// case 'r':
	// 	casio.record();
	// 	break;
	case 'b':
		casio.batteryChange();
		break;
	// case ' ':
	// 	casio.play();
	// 	break;
	// case BACKSPACE:
	// 	casio.pause();
	// 	break;
	}
}
