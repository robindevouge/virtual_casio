class Bound {

Bound(Part p){
	part = p;
}

Part part;
int x1;
int x2;
int y1;
int y2;
}

class Part {

Part(String n, String url, int x, int y, Part p){
	name = n;
	img = loadImage(url);
	// positions are relative to parent
	initialX = x;
	initialY = y;
	posX = x;
	posY = y;
	// parent should be root if is has normally no parent
	parent = p;
}

String name;
PImage img;
int initialX;
int initialY;
int posX;
int posY;
int finalX;
int finalY;
Part parent;

void display(){

	this.finalX = this.parent.finalX+this.posX;
	this.finalY = this.parent.finalY+this.posY;

	image(this.img, this.finalX, this.finalY);
}

void moveTo(int x, int y){
	this.posX = x;
	this.posY = y;
}

void moveBack(){
	this.posX = initialX;
	this.posY = initialY;
}

void changeImg(String url){
	img = loadImage(url);
}

int getX(){
	this.finalX = this.parent.finalX+this.posX;
	// println(this.finalX);
	return this.finalX;
}
int getY(){
	this.finalY = this.parent.finalY+this.posY;
	return this.finalY;
}

}





class Player {

Player() {
};

// vars
String status = "pause";
int barPosX1 = 10;
int barPosX2 = 110;
int barPosY = 100;
int barLength = barPosX2-barPosX1;
float percent = 0.5;
int time = 0;
int delta = 0;
float volume = 90;
int rewindSpeed = 120;
int forwardSpeed = 60;
String selectedTape = "custom";
float battery = 100.0;
boolean batteryDead = false;

//functions

void batteryDrain(){
	if (this.battery <= 0) {
		this.battery = 0;
		this.pause();
		this.batteryDead = true;
	}else{
		this.battery -= 0.05;
		this.batteryDead = false;
	}
}

void batteryChange(){
	this.battery = 100.0;
	this.batteryDead = false;
}

void drawBar() {
	strokeWeight(10);
	strokeCap(SQUARE);

	stroke(0, 0, 0);
	line(this.barPosX1, this.barPosY, this.barPosX2, this.barPosY);
}

void updateTime() {
	float progression = map(this.time, 0, ap.length(), this.barPosX1, this.barPosX2);
	stroke(50, 200, 200);
	line(this.barPosX1, this.barPosY, progression, this.barPosY);
}

void displayStatus() {
	text("status : "+this.status, 10, 20);
	text("volume : "+this.volume, 10, 40);
	text("tape : "+this.selectedTape, 10, 60);
	text("battery : "+this.battery, 10, 80);
}

void pause() {
	cRewind.changeImg("btn_rewind_off.png");
	cForward.changeImg("btn_forward_off.png");
	cPlay.changeImg("btn_play_off.png");
	cRec.changeImg("btn_rec_off.png");
	spring = minim.loadFile("data/env_pause.wav");
	// spring.play();

	switch(this.status) {
	case "record":
		recorder.endRecord();
		fp = new FilePlayer( recorder.save() );
		fp.patch( output );
		ap = minim.loadFile("data/tape_"+this.selectedTape+".wav");
		this.time = 0;
		spring.play();
		break;
	case "play":
		ap.pause();
		//img update
		spring.play();
		break;
	case "rewind":
		spring.play();
		loop.pause();
		break;
	case "forward":
		spring.play();
		loop.pause();
		break;
	}
	this.status = "pause";
}

void play() {
	if(!batteryDead && this.status != "play" && this.status != "record") {
		this.pause();
		env = minim.loadFile("data/env_play.wav");
		env.play();
		cPlay.changeImg("btn_play_on.png");
		ap.play(this.time);
		this.status = "play";
		//img update
	}
}

void record() {
	if(!batteryDead && this.status != "record") {
		this.pause();
		env = minim.loadFile("data/env_play.wav");
		env.play();
		cRec.changeImg("btn_rec_on.png");
		cPlay.changeImg("btn_play_on.png");
		recorder = minim.createRecorder(input, "data/tape_"+this.selectedTape+".wav");
		recorder.beginRecord();
		casio.status = "record";
	}
}

void rewind() {
	if(!batteryDead && this.status != "rewind") {
		env = minim.loadFile("data/env_btn.wav");
		env.play();
		this.pause();
		loop = minim.loadFile("data/env_rewind.wav");
		loop.play();
		loop.loop();
		cRewind.changeImg("btn_rewind_on.png");
		this.status = "rewind";
	}
}

void forward() {
	if(!batteryDead && this.status != "forward") {
		env = minim.loadFile("data/env_btn.wav");
		env.play();
		this.pause();
		loop = minim.loadFile("data/env_ff.wav");
		loop.play();
		loop.loop();
		cForward.changeImg("btn_forward_on.png");
		this.status = "forward";
	}
}

void changeTape(String i) {
	this.pause();

	this.selectedTape = i;
	this.time = 0;
	// print(this.selectedTape);
	ap = minim.loadFile("data/tape_"+this.selectedTape+".wav");

	env = minim.loadFile("data/env_changetape.wav");
	env.play();
}


void tick() {
	if (batteryDead) {this.pause();}

	switch (this.status) {
	case "record":
		this.batteryDrain();
		break;
	case "rewind":
		this.batteryDrain();
		if (this.time >= this.rewindSpeed) {
			this.time -= this.rewindSpeed;
		} else {
			this.pause();
		}
		break;
	case "forward":
		this.batteryDrain();
		if (this.time <= ap.length()-this.forwardSpeed) {
			this.time += this.forwardSpeed;
		} else {
			this.pause();
		}
		break;
	case "play":
		this.batteryDrain();
		this.time = ap.position();
		if (this.time >= ap.length()) {
			this.pause();
		}
		break;
	}

}
}
